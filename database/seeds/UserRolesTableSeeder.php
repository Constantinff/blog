<?php

use Illuminate\Database\Seeder;
use App\Models\UserRole;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRoles = [
            UserRole::ROLE_ADMIN,
            UserRole::ROLE_USER,
        ];

        foreach ($userRoles as $role) {
            UserRole::create([
                'name' => $role
            ]);
        }
    }
}
