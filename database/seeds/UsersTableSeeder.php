<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->name = 'admin';
        $admin->email = 'admin@blog.local';
        $admin->password = Hash::make('admin');
        $admin->user_role_id = UserRole::getOrFailByName(UserRole::ROLE_ADMIN)->id;

        $admin->save();
    }
}
