<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Post extends Model
{

    /**
     * DB connection
     *
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'posts';

    // public $with = ['author'];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'url',
        'body',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:n M',
    ];

    /**
    * These properties used in Vue
    *
    * @var array
    */
    protected $appends = [
        'body_short',
    ];

    /**
     * Get the post author.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get short post body.
     * @return string
     */
    public function getBodyShortAttribute()
    {
        $limit = 300;
        $plainText = strip_tags($this->body);

        return substr($plainText, 0, $limit) .
            (strlen($plainText) > $limit ? ' &hellip;' : null);
    }

}
