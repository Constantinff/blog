<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{

    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_roles';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get role by name or fail.
     *
     * @param string $name
     *
     * @return UserRole|null
     */
    public static function getOrFailByName($name)
    {
        return static::where('name', $name)->firstOrFail();
    }

}
