
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import VueRouter from 'vue-router';
import router from './routes';

window.Vue = require('vue');

Vue.use(VueRouter)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('post-preview', require('./components/PostPreview.vue'));
Vue.component('post-form', require('./components/PostForm.vue'));

const rootElement = document.getElementById('app');
const user = rootElement.getAttribute('user');

const app = new Vue({
    el: '#app',
    data: {
        user: user && JSON.parse(user)
    },
    router
});
