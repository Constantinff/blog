import VueRouter from 'vue-router';
import Blog from './components/Blog.vue';
import BlogPost from './components/BlogPost.vue';
 
const routes = [
    {
        path: '/:page?',
        name: 'blog',
        component: Blog
    },
    {
        path: '/blog/create',
        component: BlogPost
    },
    {
        path: '/blog/:postUrl',
        name: 'post',
        component: BlogPost
    },
];
 
 
export default new VueRouter({
    mode: 'history',
    routes
});